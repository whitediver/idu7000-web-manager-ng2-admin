import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgaModule } from '../../theme/nga.module';
import { TreeModule } from 'ng2-tree';
import {InlineEditorModule} from 'ng2-inline-editor';
import { routing } from './monitoring.routing';
import { Monitoring } from './monitoring.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    InlineEditorModule,
    NgaModule,
    TreeModule,
    routing
  ],
  declarations: [
    Monitoring
  ]
})
export class MonitoringModule {}
