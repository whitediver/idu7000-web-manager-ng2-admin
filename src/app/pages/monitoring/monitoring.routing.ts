import { Routes, RouterModule } from '@angular/router';

import { Monitoring } from './monitoring.component';

// noinspection TypeScriptValidateTypes
const routes: Routes = [
  {
    path: '',
    component: Monitoring
  }
];

export const routing = RouterModule.forChild(routes);
